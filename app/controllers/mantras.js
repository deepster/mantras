/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    async = require('async'),
    Mantra = mongoose.model('Mantra'),
    _ = require('underscore');


/**
 * Find mantra by title
 */
exports.mantra = function(req, res, next, title) {
    Mantra.load(title, function(err, mantra) {
        if (err) return next(err);
        if (!mantra) return next(new Error('Failed to load mantra ' + title));
        req.mantra = mantra;
        next();
    });
};

/**
 * Create a mantra
 */
exports.create = function(req, res) {            
    var mantra = new Mantra(req.body);
    mantra.author = req.user.username;

    mantra.save(function(err) {
        if (err) {
            return res.send('users/signup', {
                errors: err.errors,
                mantra: mantra
            });
        } 
        else {
            res.jsonp(mantra);
        }
    });
};

/**
 * Update a mantra
 */
exports.update = function(req, res) {
    var mantra = req.mantra;

    mantra = _.extend(mantra, req.body);

    mantra.save(function(err) {
        res.jsonp(mantra);
    });
};

/**
 * Delete an mantra
 */
exports.destroy = function(req, res) {
    var mantra = req.mantra;

    mantra.remove(function(err) {
        if (err) {
            res.render('error', {
                status: 500
            });
        } else {
            res.jsonp(mantra);
        }
    });
};

/**
 * Show an mantra
 */
exports.show = function(req, res) {
    res.jsonp(req.mantra);
};

/**
 * List of Mantras
 */
exports.all = function(req, res) {
    Mantra.find().exec(function(err, mantras) {
        if (err) {
            res.render('error', {
                status: 500
            });
        } else {
            res.jsonp(mantras);
        }
    });
};
