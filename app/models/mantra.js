/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    config = require('../../config/config'),
    Schema = mongoose.Schema;

/**
 * Mantra Schema
 */
var MantraSchema = new Schema({
    href: {
        type: String,
        default: ''
    },
    title: {
        type: String,
        default: 'Dummy',
        trim: true
    },
    heading: {
        type: String,
        default: 'Dummy',
        trim: true
    },
    content: {
        type: String,
        default: '',
        trim: true
    },
    author: {
        type: String,
        default: '',
        trim : true
    }
});

/**
 * Validations
 */
MantraSchema.path('title').validate(function(title) {
    return title.length;
}, 'Title cannot be blank');

/**
 * Statics
 */
MantraSchema.statics = {
    load: function(title, cb) {
        console.log(" Load function called with title : " + title);
        var tt = title.replace('+',' ');
        this.findOne({
            title: title
        }).exec(cb);
    }
};

mongoose.model('Mantra', MantraSchema);