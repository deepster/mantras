# Programming Mantras 
  *Dispersing programming Mantras since like right now!*

  A weekend hack to explore the power of a complete javascript based stack. 

## Idea

Stumbled upon [97 things](http://programmer.97things.oreilly.com/wiki/index.php/Contributions_Appearing_in_the_Book) the other day while surfing the social webs. It seemed a great source of gyan but shackled by an old and clunky interface, so I decided to spend my weekend redesigning it for the mordern user with a huge focus on readablity. Also to test out the all-javascript stack.

## Scraping

To get the content out of the wiki I used  [node.io](https://github.com/chriso/node.io/wiki) to wrote a few scraping jobs. They extract the links, author and content and put it all in a local mongodb collection

## End-to-End Web app

With the content safely in the database all I need is to bootstrap a Web app to serve these mantras aesthetically .The  MEAN ([Mongo](http://www.mongodb.org), [Express](http://expressjs.com), [Angular](http://angularjs.org) and [Node](http://www.nodejs.org))is perfect for this. Created the required MVC components for the Mantras on Angular and Express and conneted it to the MongoDB mantras collection already extracted. 


## Deployment

[Heroku](https://www.heroku.com) to the rescue! Super quick setup and deployment. Setup the the MongoHQ addon for the database and uploaded my local collection data to MongoHQ
and voila  - [Programming Mantras](http://mantras.pradeepvarma.in)

## Roadmap

Want to add few more features to the App over the next few weeks
  * A subscription service which will email a random mantra once a week
  * User Accounts and login through Facebook, Twitter and Google+, and ability for authenticated users to add Mantras

## Credits

  * The invaluable resource for Mantras - [97 Things every programmer should know](sublprogrammer.97things.oreilly.com/wiki/index.php/Contributions_Appearing_in_the_Book). 
  * MEAN stack from [mean.io](http://mean.io)


