//Setting up route
window.app.config(['$routeProvider',
    function($routeProvider) {
        $routeProvider.
        when('/articles', {
            templateUrl: 'views/articles/list.html'
        }).
        when('/articles/create', {
            templateUrl: 'views/articles/create.html'
        }).
        when('/articles/:articleId/edit', {
            templateUrl: 'views/articles/edit.html'
        }).
        when('/articles/:articleId', {
            templateUrl: 'views/articles/view.html'
        }).
        when('/mantras', {
            templateUrl: 'views/mantras/list.html'
        }).
        when('/mantras/create', {
            templateUrl: 'views/mantras/create.html'
        }).
        when('/mantras/:mantraTitle/edit', {
            templateUrl: 'views/mantras/edit.html'
        }).
        when('/mantras/:mantraTitle', {
            templateUrl: 'views/mantras/view.html'
        }).
        when('/', {
            templateUrl: 'views/index.html'
        }).
        otherwise({
            redirectTo: '/'
        });
    }
]);

//Setting HTML5 Location Mode
window.app.config(['$locationProvider',
    function($locationProvider) {
        $locationProvider.hashPrefix("!");
    }
]);