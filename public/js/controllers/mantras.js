angular.module('mean.mantras').controller('MantrasController', ['$scope', '$routeParams', '$location', 'Global', 'Mantras', function ($scope, $routeParams, $location, Global, Mantras) {
    $scope.global = Global;
    $scope.create = function() {
        var mantra = new Mantras({
            title: this.title,
            content: this.content
        });
        mantra.$save(function(response) {
            $location.path("mantras/" + mantra.title);
        });

        this.title = "";
        this.content = "";
    };

    $scope.remove = function(mantra) {
        mantra.$remove();  

        for (var i in $scope.mantras) {
            if ($scope.mantras[i] == mantra) {
                $scope.mantras.splice(i, 1);
            }
        }
    };

    $scope.update = function() {
        var mantra = $scope.mantra;
        if (!mantra.updated) {
            mantra.updated = [];
        }
        mantra.updated.push(new Date().getTime());

        mantra.$update(function() {
            $location.path('mantras/' + mantra.title);
        });
    };

    $scope.find = function(query) {
        Mantras.query(query, function(mantras) {
            $scope.mantras = mantras;
        });
    };

    $scope.findOne = function() {
        Mantras.get({
            mantraTitle: $routeParams.mantraTitle
        }, function(mantra) {
            $scope.mantra = mantra;
        });
    };
}]);