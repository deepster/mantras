window.app = angular.module('mean', ['ngCookies', 'ngResource', 'ngSanitize', 'ui.bootstrap', 'ui.route', 'mean.system', 'mean.articles', 'mean.mantras']);

angular.module('mean.system', []);
angular.module('mean.articles', []);
angular.module('mean.mantras', []);