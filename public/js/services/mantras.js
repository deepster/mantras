//Mantras service used for mantras REST endpoint
angular.module('mean.mantras').factory("Mantras", ['$resource', function($resource) {
    return $resource('mantras/:mantraTitle', {
    }, {
        update: {
            method: 'PUT'
        }
    });
}]);