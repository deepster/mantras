;var nodeio = require('node.io'),
    mongo = require('mongodb').MongoClient,
    format = require('util').format;

var options = {
    timeout : false,    // Timeout after 10 secs
    max     : 3,    // run  not more than 20 threads asynchronously
    retries : 3     // Threads can retry 3 times before giving up
}

exports.job = new nodeio.Job(options, {
    input : false,
    run : function(row){
        var job = this;
        var last_job;
        mongo.connect('mongodb://127.0.0.1:27017/scraping', function(err, db) {
            if(err){
                console.log("Unable to connect to Mongo DB : " +  err);
                throw err;
            } 
            console.log("Able to connect to Mongo DB : ");

            var collection = db.collection('mantras');
            collection.find({},function (err, docs) {
                if(err){
                    console.log("Unable fetch mantras from Mongo DB : " +  err);
                    throw err;
                } 
                docs.count(function (err, count) {
                    console.log("Able to fetch " + count + " mantras from Mongo DB  ");
                });

                docs.each(function (err, doc) {
                    if(err){
                        console.log("Unable fetch one mantra from Cursor : " +  err);
                        throw err;
                    }
                    if (doc == null){
                        console.log("Captured all the Mantras in the DB");
                        return;
                    }
                    last_job = doc._id;
                    console.log("Able to fetch one mantra from Cursor : " +  doc.title);

                    job.getHtml(doc.href, function (err, $) {
                        if(err){
                            console.log("Unable fetch the HTML page for " + doc.title + " : " +  err);
                            throw err;
                        } 
                        if($){
                            var bodyContent = $('#bodyContent');
                           
                            var content = "", prev = "", prevprev = "";

                            if(bodyContent){

                                bodyContent.children.each(function  (node) {
                                    if( node.type === "comment" ||
                                        (   node.attribs && 
                                            (   node.attribs.class === "visualClear" ||
                                                node.attribs.class === "printfooter" ||
                                                node.attribs.id === "jump-to-nav" ||
                                                node.attribs.id === "contentSub" ||
                                                node.attribs.id === "siteSub"
                                            )
                                        )
                                    ){
                                        return;
                                    }


                                    prevprev = prev;
                                    prev = content;
                                    content += node.innerHTML;
                                });
                                
                                doc.content = prevprev;

                                collection.save(doc, function (err, result) {
                                    if(err){
                                        console.log("Unable save the mantra content into Mongo DB : " +  err);
                                        throw err;
                                    }
                                    if(result == 1){
                                        console.log("Updating Mantra : " + doc.title);
                                    } else {
                                        console.log("Adding Mantra : " + result);
                                    }
                                    if(last_job === doc._id){
                                        db.close();
                                        job.emit();
                                    }
                                });
                            }
                        }else {
                            console.log("Invalid response from server");
                        }
                    });

                });
            });

        });
    }
});