
var fetch_mantra = require('./fetch_mantra').job,
    nodeio = require('node.io'),
    mongo = require('mongodb').MongoClient,
    format = require('util').format;

var options = {
    timeout : 10,   // Timeout after 10 secs
    max     : 20,   // run  not more than 20 threads asynchronously
    retries : 3     // Threads can retry 3 times before giving up
}

exports.job = new nodeio.Job(options, {
    input : false,
    run : function(){
        var base_url = 'http://programmer.97things.oreilly.com';
        var request = base_url + '/wiki/index.php/Contributions_Appearing_in_the_Book';
        var job = this;

        console.log('Processing mantras : ' + request);
        job.getHtml(request, function (err, $) {
            var result = [];
            if($){
                $('#bodyContent li').each(function(li){
                    var title = li.children[0].attribs.title;
                    title = title.replace(/&quot;/g,'');
                    var heading = title;
                    title = title.replace(/\?/g,'');
                    title = title.replace(/ /g, '-');
                    title = title.replace(/'/g,'');
                    title = title.replace(/,/g,'');
                    var mantra = {
                        title : title,
                        heading : heading,
                        href : base_url + li.children[0].attribs.href,
                        author :  li.children[2].attribs.title,
                        content :""
                    };
                    result.push(mantra);
                });
                mongo.connect('mongodb://127.0.0.1:27017/scraping', function(err, db) {
                    if(err){
                        console.log("Unable to connect to Mongo DB : " +  err);
                        throw err;
                    } 
                    console.log("Able to connect to Mongo DB : ");

                    var collection = db.collection('mantras');
                    collection.remove(function (err, count) {
                        if(err){
                            console.log("Unable to clean the 'mantras' collection  : " +  err);
                            throw err;
                        }else {
                            console.log( 'Able to remove ' + count + ' recods from the database');
                            collection.insert(result, function (err, docs) {
                                if(err){
                                    console.log("U to insert links to Mongo DB : " +  err);
                                    throw err;
                                } 

                                db.close();
                                job.emit("Able to insert all mantras to Mongo DB  ");
                            });
                        }
                    });

                });
            }
        });
    }
});